from bokeh.io import output_notebook, show
from math import pi
import numpy as np
import pandas as pd
from bokeh.plotting import figure

def isnotebook():
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter

# Load the data in the df approach 2: load from Yahoo Finance
from pandas_datareader import data as pdr

import fix_yahoo_finance as yf
yf.pdr_override() # <== that's all it takes :-)

from datetime import datetime

def read_yahoo_as_csv(stockCode, nrows=350):
    today = datetime.today().strftime('%Y-%m-%d')
    df = pdr.get_data_yahoo(stockCode, start="2018-11-01", end=today)
    df=df.round(3)
    #df = pd.DataFrame(df,columns=['Open','High','Low','Close','Volume'])
    df = pd.DataFrame(df)
    df.reset_index(inplace=True)
    #df.index
    #df.columns
    #df.tail()
    return df

def get_stock_data_dict(df):
    df['Date'] =  df['Date'].map(lambda x: x.strftime('%m/%d/%y'))
    df = df.fillna(0)
    df = df.set_index(df['Date'])
    df = df.drop('Date', axis = 1)
    df = df.round(2)
    return df.T.to_dict('dict')

# Let's see a couple of entries in this dictionary:
#df = pd.read_csv("./data/spy.csv", nrows=2)
#df = reset_date_index(df)
#dic = get_stock_data_dict(df)
#dic

# The indicators are added to the dataframe itself through the functions below.  
def ema(df, n):
    price = df['Close']
    price = price.fillna(method='ffill')
    EMA = pd.Series(price.ewm(span = n, min_periods = n - 1).mean(), name = 'EMA_' + str(n))
    df = df.join(EMA)
    return df

def bollinger(df, n):
    price = df['Close']
    price = price.fillna(method='ffill')
    numsd=2
    """ returns average, upper band, and lower band"""
    df['bbupper_' + str(n)] = price.ewm(span = n, min_periods = n - 1).mean() + 2 * price.rolling(min_periods=n,window=n,center=False).std() 
    df['bblower_' + str(n)] = price.ewm(span = n, min_periods = n - 1).mean() - 2 * price.rolling(min_periods=n,window=n,center=False).std() 
    
    return df
    

# Interpret the indicator parameters and add the indicators to the chart:
def add_indicators(indicators_params_list, df, chart):
    for indicator_params in indicators_params_list:
        if indicator_params['name'] == 'ema':
            period = indicator_params['period']
            df = ema(df, period)
            chart.line(df.Date, df['EMA_' + str(period)], line_dash=(4, 4), color='black', alpha=0.7, legend = 'EMA ' + str(period))
        elif indicator_params['name'] == 'bollinger':
            period = indicator_params['period']
            df = bollinger(df, period)
            chart.line(df.Date, df['bbupper_' + str(period)], color='red', alpha=0.7, legend = 'bbupper ' + str(period))
            chart.line(df.Date, df['bblower_' + str(period)], color='black', alpha=0.7, legend = 'blower ' + str(period))
        
    return chart
          
from bokeh.models import Label
from bokeh.models.formatters import DatetimeTickFormatter
from bokeh.models import HoverTool, CustomJS

# This function sets the date as the index of this dataframe.  
# In case of missing days, it also fills them with nan.
def reset_date_index(df):
    df["Date"] = pd.to_datetime(df["Date"])
    new_dates = pd.date_range(df.Date.min(), df.Date.max())
    df.index = pd.DatetimeIndex(df.Date)
    df = df.reindex(new_dates, fill_value=np.nan)
    df['Date'] = new_dates
    return df

def get_stock_chart(stock_data, chart_params):
    
    # Reset the date index.
    stock_data = reset_date_index(stock_data)
    
    # Only keep the number of days requested in chart_params
    stock_data = stock_data.tail(chart_params['days'])
    
    # Make a Bokeh figure
    # Bokeh comes with a list of tools that include xpan and crosshair.
    # TODO: make xpan work well
    #TOOLS = "xpan,crosshair"
    TOOLS = "crosshair"    
    p = figure(x_axis_type='datetime', tools=TOOLS, plot_width=chart_params['size']['width'], plot_height= chart_params['size']['height'], title = chart_params['title'])
    p.xaxis.major_label_orientation = pi/4
    p.grid.grid_line_alpha=0.3
    
    mids = (stock_data.Open + stock_data.Close)/2
    spans = abs(stock_data.Close-stock_data.Open)
    inc = stock_data.Close > stock_data.Open
    dec = stock_data.Open >= stock_data.Close
    half_day_in_ms_width = 12*60*60*1000 # half day in 

    # Bokeh glyphs allows you to draw different types of glyphs on your charts....
    # Each candle consists of a rectangle and a segment.  
    p.segment(stock_data.Date, stock_data.High, stock_data.Date, stock_data.Low, color="black")
    # Add the rectangles of the candles going up in price
    p.rect(stock_data.Date[inc], mids[inc], half_day_in_ms_width, spans[inc], fill_color=chart_params['colors']['up'], line_color="black")
    # Add the rectangles of the candles going down in price
    p.rect(stock_data.Date[dec], mids[dec], half_day_in_ms_width, spans[dec], fill_color=chart_params['colors']['down'], line_color="black")
    
    ############# ADDING INDICATORS ############################
    p = add_indicators(chart_params["indicators"], stock_data, p)
    p.legend.location = "top_left"
    
    ############# ADDING HOVER CALLBACK ############################
    # Create a dictionary that I can pass to the javascript callback
    stock_data_dictio = get_stock_data_dict(stock_data)
    
    callback_jscode = """
    var stock_dic = %s;         //The dictionary will be replaced here
    var day_im_ms = 24*60*60*1000;
    
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return [ month, day, year.toString().substring(2)].join('/');
    }
     
     var d = cb_data.geometry.x;
     try {
      d = Math.floor( d + day_im_ms);
      d = new Date(d);
    } catch(err) {
       d= err; 
    }

    sel_date = formatDate(d);
    
    date_lbl = sel_date;
    date_lbl = date_lbl + " open:" + stock_dic[sel_date].Open
    date_lbl = date_lbl + " close:" + stock_dic[sel_date].Close
    date_lbl = date_lbl + " high:" + stock_dic[sel_date].High
    date_lbl = date_lbl + " low:" + stock_dic[sel_date].Low
    date_label.text = date_lbl
    """  % stock_data_dictio   # <--- Observe tha dictionary that is to be replaced into the stock_dic variable

    # This label will display the date and price information:
    #date_label = Label(x=30, y=chart_params['size']['height']-50, x_units='screen', y_units='screen',
    date_label = Label(x=230, y=chart_params['size']['height']-60, x_units='screen', y_units='screen',
                     text='', render_mode='css',
                     border_line_color='white', border_line_alpha=1.0,
                     background_fill_color='white', background_fill_alpha=1.0)

    date_label.text = ""
    p.add_layout(date_label)
    
    # When we create the hover callback, we pass the label and the callback code.
    callback = CustomJS(args={'date_label':date_label}, code=callback_jscode)
    p.add_tools(HoverTool(tooltips=None, callback=callback))
    ###################################################################   

    return p

###############################################################################
if isnotebook(): 
    print("I am in Notebook")
    output_notebook()
else:
    print("I am in std interpreter")

#df = pd.read_csv("./data/spy.csv", nrows=350)
stockCode="0388.HK"
daysToShow=150
df=read_yahoo_as_csv(stockCode)
print(df.tail())

chart_params = {
        "title" : stockCode,
        "colors" : {"up":"Green", "down": "Red"},
        "size" : {"height": 500 ,"width": 900},
        "days" : daysToShow,
    
        "indicators" : [
          {"name":"ema", "period": 20},
          #{"name":"ema", "period": 5},
          {"name":"bollinger", "period": 20}
    ]
}

p= get_stock_chart(df, chart_params)

if isnotebook(): 
    print("I am in Notebook, show chart")
    show(p)
else:
    print("I am in std interpreter, write file")
    from bokeh.resources import CDN
    from bokeh.embed import file_html

    myplot_html = file_html(p, CDN, "0388.HK Cross Hair Chart")
    with open('crossHair.html', 'w') as file:
        file.write(myplot_html)

    # this HTML code is very long (~30 K), the cell below doesn't show all the code in NBviewer
    #print(myplot_html)
